$j(document).ready(function() {
    var $bundleDropdowns = $j('select[name^="bundle_option"], input[name^="bundle_option"][type="checkbox"]');
    if ($bundleDropdowns.length > 0) {
    	$bundleDropdowns.change(bundle_review_handler);
    }
});

$j(window).load(function() {
    bundle_review_handler();
});

 function bundle_review_handler() {
 	var $bundleDropdowns = $j('select[name^="bundle_option"], input[name^="bundle_option"][type="checkbox"]');
        var bids = '';
        $j.each($bundleDropdowns, function(i, dropDown) {
        	$dropDown = $j(dropDown);
        	var bundleId = /.*?\[(.*?)\]/.exec($dropDown.attr('name'))[1];
        	var selectedId = $dropDown.prop('tagName') == "INPUT" && $dropDown.is(":checked") ? $dropDown.val() : $dropDown.find(':selected').val();
        	bids += bundleId + '-' + selectedId + ',';
        });
        bids = bids.substring(0, bids.length - 1);

        var url = 'http://' + document.location.hostname + '/feeforeviews/index/sku/pid/' + bundle.config.bundleId + '/bids/' + bids;

        $j.ajax({
        	url: url
        }).done(function(data) {
        	$j('#product-feefo-reviews tbody').html(data);
        });
    }