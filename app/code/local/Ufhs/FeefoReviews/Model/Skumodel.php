<?php

/**
 * Feefo Reviews Model
 * 
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_FeefoReviews_Model_Skumodel extends Mage_Core_Model_Abstract
{
    /**
     * Generate the product SKU
     *
     * This function takes the product and bundle option IDs and converts them
     * into the sku associated to that product permutation.
     * 
     * @param  Array
     * @return String
     */
    public function getRealSku($params)
    {
        return $this->getProdSku($this->getSku($params));
    }
    
    /**
     * Convert the provided parameters into the assigned SKU
     *
     * This function takes the product and bundle option IDs and generates the
     * SKU mapped to that particular product
     * 
     * @param  Array
     * @return String
     */
    private function getSku($params)
    {
        if(!$this->validateParams($params))
        {
            throw new Exception('Invalid parameters have been passed.');
        }

        $bidGroups = explode(',', $params['bids']);
        foreach ($bidGroups as $group)
        {
            list($bundleId, $optionId) = explode('-', $group);
            $bids[$bundleId] = $optionId;
        }

        $product = Mage::getModel('catalog/product')->load($params['pid']);

        if(!$product->getId())
        {
            throw new Exception("Invalid product ID has been passed.");
        }

        // Get list of options from product
        $typeInstance = $product->getTypeInstance(true);
        $typeInstance->setStoreFilter($product->getStoreId(), $product);

        $selectionCollection = $typeInstance->getSelectionsCollection($typeInstance->getOptionsIds($product), $product);

        $optionCollection = $typeInstance->getOptionsCollection($product);
        $options = $optionCollection->appendSelections($selectionCollection, false, Mage::helper('catalog/product')->getSkipSaleableCheck());

        // Concatanate the skus from the found options
        $sku = $product->getData('sku');
        foreach ($bids as $bundleId => $optionId)
        {
            foreach ($options[$bundleId]->getData('selections') as $option)
            {
                $data = $option->getData();
                if ($data['selection_id'] == $optionId)
                {
                    $sku .= '-' . $data['sku'];
                }
            }
        }
        return $sku;
    }

    /**
     * Lookup the SKU
     *
     * This function takes the Magento 'fake' SKU and looks up the 
     * corresponding product SKU.
     * 
     * @param  String
     * @return String
     */
    private function getProdSku($sku)
    {
        $skuTable = Mage::getModel('feeforeviews/skus')->load($sku, 'realsku');
        $ekmSku = $skuTable->getEkmsku();        
        if(!$ekmSku)
        {
            throw new Exception("An error has occurred locating that particular product within our database.");
        }
        return $ekmSku;
    }   

    /**
     * Validate parameters
     *
     * This function takes the paramters that were passed in the URL and 
     * checks firstly to see if they exist, then validates the values assigned
     * to them.
     * 
     * @param  Array
     * @return Boolean
     */
    private function validateParams($params)
    {
        if(!isset($params['pid']) || !isset($params['bids']))
        {
            return false;
        }
        if(!is_numeric($params['pid'])){
            return false;
        }
        if(!preg_match('/^[-,a-z0-9]+$/',$params['bids'],$valid))
        {
            return false;
        }
        foreach(explode(",",$params['bids']) as $entry)
        {
            if(count(explode('-',$entry)) != 2)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieve Feefo reviews
     *
     * This function takes the provide SKU and contacts Feefo to return the 
     * reviews assigned to that product.
     * 
     * @param  String
     * @return Object
     */
    public function getFeefoReviews($sku)
    {
        // Instantiate the Flint Feefo product reviews class
        $feefoabstract = Mage::getSingleton('Flint_Feefo_Block_Reviews_Product_Page');
        // Load the Flint Feefo reviews helper
        $helper = Mage::helper('flint_feefo/reviews');
        // Build the Feefo review URL, passing the new SKU into vendorref
        $data = array(
            '_escape' => true,
            'logon' => $feefoabstract->getLogon(),
            'vendorref' => $sku,
            'mode' => $feefoabstract->getMode(),
            'order' => $feefoabstract->getOrder(),
            'since' => $feefoabstract->getSince(),
            'limit' => $feefoabstract->getLimit(),
            );
        // Contact the Feefo API to return a SimpleXMLElement of the reviews relating that product
        $reviews = $helper->getReviews($feefoabstract::REVIEWS_BASE_URL . '?' . http_build_query( $data ) . $feefoabstract->getAdditional());
        // Convert the SimpleXMLElement into a usable object and return that object
        if(!$reviews)
        {
            throw new Exception('Unable to communicate with Feefo server. Please try again at a later time.');
        }
        return $reviews->xpath("//FEEDBACK");

    }

    /**
     * Display reviews from Feefo
     *
     * This function takes the reviews that it received form Feefo and outputs
     * them in a consist format to Feefo's.
     * 
     * @param  Object
     * @return HTML
     */
    public function getReviewTemplate($reviews)
    {
        // Load the Mage App class
        return Mage::app()
        // Get the current layout
        ->getLayout()
        // Load the product reviews block
        ->createBlock('feeforeviews/product')
        // Passing it the reviews object via the feedbacks variable
        ->setData('feedbacks',$reviews)
        // Set the template of that block to be the reviews template
        ->setTemplate('feeforeviews/reviews.phtml')
        // Return the HTML rendition of this template
        ->toHtml();        
    }
}