<?php

/**
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_FeefoReviews_Model_Resource_Skus extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('feeforeviews/skus', 'id');
    }

}