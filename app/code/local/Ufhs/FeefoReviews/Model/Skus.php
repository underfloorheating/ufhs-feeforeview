<?php

/**
 * 
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 * 
 * @method int getImageId()
 * @method string getImageName()
 */
class Ufhs_FeefoReviews_Model_Skus extends Mage_Core_Model_Abstract
{
	public function _construct()
    {
        parent::_construct();
        $this->_init('feeforeviews/skus');
    }

}