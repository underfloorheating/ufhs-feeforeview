<?php
/**
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

$installer = $this;

$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$installer->getTable('feeforeviews/skus')};

    CREATE TABLE {$installer->getTable('feeforeviews/skus')} (
        `id` int(11) unsigned NOT NULL auto_increment, 
        `realsku` varchar(255) NOT NULL default '',  
        `ekmsku` varchar(255) NOT NULL default '',                     
        PRIMARY KEY (`id`),
        UNIQUE KEY `realsku` (`realsku`) 
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();