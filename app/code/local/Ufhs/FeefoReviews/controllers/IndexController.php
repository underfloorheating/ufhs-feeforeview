<?php

/**
 * Feefo Reviews Controller
 *
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_FeefoReviews_IndexController extends Mage_Core_Controller_Front_Action
{
	public function skuAction()
	{		
		try 
		{
			// Make sure all required modules and resources are available
			try 
			{
				Mage::getSingleton('Flint_Feefo_Block_Reviews_Product_Page');
				Mage::helper('flint_feefo/reviews');
				Mage::getModel('feeforeviews/skus');
				Mage::getModel('feeforeviews/skumodel');
				Mage::getModel('catalog/product');
				Mage::helper('catalog/product');
			}
			catch (Exception $e)
			{
				throw new Exception('An error occured loading the requested resources.');
			}
			
			$skuModel = Mage::getModel('feeforeviews/skumodel');
			$params = $this->getRequest()->getParams();
			// Generate the real sku from the parameters
			$sku = $skuModel->getRealSku($params);
			// Generate the reviews object from the sku
			$reviews = $skuModel->getFeefoReviews($sku);
			// Display the reviews
			echo $skuModel->getReviewTemplate($reviews);
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
			return false;
		}		
	}
}