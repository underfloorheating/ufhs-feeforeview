<?php

/**
 * Feefo Reviews Block Model
 * 
 * @version     $Id$
 * @package     Ufhs_FeefoReviews
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_FeefoReviews_Block_Product extends Mage_Core_Block_Template
{
	public function getReviewObject()
	{
		// Take the reviews object and return it to the calling variable in the template
		return $this->feedbacks;
	}
}
